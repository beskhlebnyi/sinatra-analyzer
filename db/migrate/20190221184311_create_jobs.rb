class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :location
      t.string :salary
      t.string :type
      t.string :company
      t.string :link
      t.text   :description
      t.datetime :posted_at

      t.timestamps
    end
    
  end
end
