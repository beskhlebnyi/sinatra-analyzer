class AddSourceToJobs < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :source, :string
  end
end
