require 'sinatra'
require 'sinatra/namespace'
Dir["./app/services/*.rb"].each(&method(:require))
require './app/controllers/application_controller.rb'
Dir["./app/controllers/*.rb"].each(&method(:require))
require 'sinatra/activerecord'
require 'faraday'
require 'multi_json'

Dir["./app/models/*.rb"].each(&method(:require))

use ApplicationController
use JobsController

set :database_file, 'config/database.yml'
