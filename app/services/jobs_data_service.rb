module JobsDataService
  def get_github_jobs(title)
    url = "https://jobs.github.com"

    conn = Faraday.new(url: url) do |faraday|
      faraday.adapter Faraday.default_adapter
    end

    response = conn.get('positions.json', utf8: '%E2%9C%93', description: title)
    parsed_body = JSON.parse(response.body)
  end

  def get_jooble_jobs(title)
    url = "https://us.jooble.org/api/8c657434-1e29-475f-95ab-9fe20e061bf2"
    response = Faraday.post do |req| 
      req.url url
      req.body = "{'keywords': '#{title}'}"
    end
    
    parsed_body = JSON.parse(response.body)
  end
end
