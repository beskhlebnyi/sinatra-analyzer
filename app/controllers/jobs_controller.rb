class JobsController < ApplicationController
  include JobsDataService
  register Sinatra::Namespace
  
  namespace '/api/v1' do
    before do
      content_type 'application/json'
    end

    get "/jobs/:title" do |title|
      jobs = []
      github_jobs = get_github_jobs(title)
      jooble_jobs = get_jooble_jobs(title)

      github_jobs[0...10].each do |job|
        jobs << Job.new(
          title:       job["title"],
          location:    job["location"],
          job_type:    job["type"],
          company:     job["company"],
          link:        job["url"],
          description: job["description"],
          posted_at:   job["created_at"],
          source:      "github_jobs"
        )
      end

      jooble_jobs["jobs"][0...10].each do |job|

        jobs << Job.new(
          title:       job["title"],
          salary:      job["salary"],
          location:    job["location"],
          job_type:    job["type"],
          company:     job["company"],
          link:        job["url"],
          description: job["snippet"],
          posted_at:   job["updated"],
          source:      "jooble"
        )
      end

      jobs.to_json
    end

    post "/job/save" do
      new_job = MultiJson.load(request.body.read)
      job = Job.new(new_job)
      job.to_json if job.save
    end

    get "/jobs" do
      Job.all.to_json
    end

    get "/job/:id" do
      job = Job.find_by(id: params[:id])
      job ? job.to_json : halt(404)
    end

    delete "/job/:id" do
      job = Job.find_by(id: params[:id])
      job ? job.destroy : halt(410)
    end

  end
end
